<?php

namespace App\Http\Controllers;

use App\Models\ParseMiddleware;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    //
    public function index(Request $request)
    {
        return view('pages.login');
    }

    public function login(Request $request)
    {
        $rules = [
            "username" => 'required|min:3',
            "password" => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect()->route("login")
                ->withErrors($validator)
                ->withInput($request->all());
        } else {
            $body = [
                "username" => $request->username,
                "password" => $request->password
            ];

            $response = ParseMiddleware::login($body);
            var_dump($response);
        }
    }
}
